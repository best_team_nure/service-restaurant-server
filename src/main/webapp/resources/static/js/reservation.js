$("document").ready(function () {
    Reservation();
    var Time = new Date();
    var hours = addZero(Time.getHours());
    var time = document.getElementById("time").value = hours + ':00';
    document.getElementById("date").min = document.getElementById("date").value;
    var check = isLogedIn();

    if (check) {
        var q = document.getElementById('HaveUser');
        q.style.visibility = 'hidden';
        var nameFromLocal = JSON.parse(localStorage.getItem("user")).name;
        document.getElementById('name').value = nameFromLocal;
        var phoneFromLocal = JSON.parse(localStorage.getItem("user")).phone;
        if(phoneFromLocal == undefined){
            document.getElementById('phone').value = "";
        }
        else {
        document.getElementById('phone').value = phoneFromLocal;
        }
    }
});

async function Reservation(check) {
    var Date = document.getElementById("date").value;
    try {
        await startSession();
        await showReservedTable(Date);
    } catch (error) {
        console.error(error);
    }

    function startSession() {
        return fetch("/session/start", {
            credentials: "same-origin"
        });
    }

    function showReservedTable(Date) {
        return fetch('/reserved/' + Date + '/date', {
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: "same-origin"
        })
            .then((response) => {
            return response.text();
    })
    .then((res) => {
            console.log(res);
        resObj = JSON.parse(res);
        console.log(resObj);
        return resObj;
    })
    .then((resObj) => {
            blockRadioButton(resObj);
    });
    }

}

function UpdateshowReservedTable(Date) {
    var Date = document.getElementById("date").value;
    return fetch('/reserved/' + Date + '/date', {
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: "same-origin"
    })
        .then((response) => {
        return response.text();
})
.then((res) => {
        console.log(res);
    resObj = JSON.parse(res);
    console.log(resObj);
    return resObj;
})
.then((resObj) => {
        blockRadioButton(resObj);
});
}

function blockRadioButton(resObj) {
    for (var x = 1; x < 22; x++) {
        var flag = false
        var currentRB = document.getElementById('radio' + x).id;
        var currentTime = document.getElementById('time').value + ':00';
        console.log(currentTime);
        for (var y = 0; y < resObj.length; y++) {
            var tempID = 'radio' + resObj[y]['tableItem'];
            var tempTIME = resObj[y]['time'];
            if (currentRB == tempID && currentTime == tempTIME) {
                document.getElementById('radio' + x).disabled = true;
                console.log('radio' + x + ' is blocked');
                flag = true;
            }
        }
        if (flag != true) {
            $("#my_select").append($('<option value="' + x + '">' + x + ' столик</option>'));
        }
    }
}

function unlockRadioButton() {
    $('input[class="rb"]').prop('disabled', false);
    $("#my_select").empty();
    console.log('all RB is unlocked!');
}

function changed_date() {
    var Date = document.getElementById("date").value;
    console.log(changed_date);
    unlockRadioButton();
    try {
        UpdateshowReservedTable(Date);
        setOptionToSelect()
    } catch (error) {
        console.error(error);
    }
    console.log("Date is changed!")
}

function changed_time() {
    var Time = document.getElementById("time").value;
    console.log(changed_time);
    unlockRadioButton();
    try {
        UpdateshowReservedTable(Date);
        setOptionToSelect()
    } catch (error) {
        console.error(error);
    }
    console.log("Time is changed!")
}

function setOptionToSelect() {
    var z = $('input[name=option]:checked').val();
    console.log(z)
    $("#my_select [value=" + z + "]").attr("selected", "selected");
}

function setRadioButtonFromSelect() {
    var y = $("#my_select option:selected").val();
    console.log(y)
    $('#radio' + y).prop('checked', true);
}

function ConfirmReservation() {
    if (checkFields()) {
        var Date = document.getElementById("date").value;
        var Time = document.getElementById("time").value + ":00";
        var table = $("#my_select option:selected").val();
        var comments = document.getElementById("comments").value;
        var phone = document.getElementById("phone").value;
        var name = document.getElementById("name").value;
        var item = '{"tableItem": { "id":' + table + '},"date":"' + Date + '","time":"' + Time + '","name":"'+name+'", "phone":"'+ phone + '","comment":"' + comments + '"}';

        sendReservation(item)
    } else {
        alert("Не все поля были заполнены верно, попробуйте еще раз.");
    }
}

function sendReservation(item) {
    fetch("/reserved/add", {
        credentials: "include",
        method: "POST",
        headers: {
            Accept: "application/json;",
            "Content-Type": "application/json;"
        },
        body: JSON.stringify(item)
    }).then(res => res).then((response) => { if(response.status == 500){console.log(item); alert("Упс, что-то пошло не так :)")}else {console.log(item);}});
}

function checkFields() {
    var isName = /^[a-zA-Zа-яА-ЯёЁ ]{2,25}$/.test(document.getElementById("name").value);
    var isTel = /^(\+380)([0-9]){9}$/.test(document.getElementById("phone").value);
    var Time = new Date();
    var hours = addZero(Time.getHours());
    var time = (hours-1) + ':00:00';

    var isTime = document.getElementById("time").value > time;
    if (isTime == false){alert('Не правильно введенное время, вы не можете осуществить бронь в прошедшем времени.')}
    if (isTel == false){alert('Не правильно введенный телефон.')}
    if (isName == false){alert('Не правильно введенное имя, пожалуйста, проверьте правильность написания.')}
    return isName && isTel && isTime;
}


function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}